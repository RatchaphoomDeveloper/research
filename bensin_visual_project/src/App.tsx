import { useState, useEffect } from "react";
import reactLogo from "./assets/react.svg";
import { CategoryData } from "./mock/category.mock";
import { CategoryInterface } from "./interface/category.interface";
import ListBoxComponent from "./component/ListBoxComponent";
import { DataInterface } from "./interface/data.interface";
import { TableColumn } from "react-data-table-component";
import TableComponent from "./component/TableComponent";
import { Bensin95Data, E20Data } from "./mock/data.mock";
import BarchartComponent from "./component/BarchartComponent";
import { ChartE20Data, Chart95Data, ChartE20DataSingle, Chart95DataSingle, ChartE20DataDouble, Chart95DataDouble, ChartE20DataDecom, Chart95DataDecom } from "./mock/chart_data.mock";

function App() {
  const [selectedCategory, setSelectedCategory] = useState<CategoryInterface>(
    CategoryData[0]
  );
  const [Datas, setDatas] = useState<DataInterface[]>([]);
  const [totalRows, setTotalRows] = useState<number>(0);
  const dataColumns: TableColumn<DataInterface>[] = [
    {
      name: "Id",
      selector: (row, index): any => (index as unknown as number) + 1,
      width: "70px",
      center: true,
      sortable: true,
      wrap: true,
    },
    {
      name: "Name",
      selector: (row, index): any => selectedCategory.name,
      width: "auto",
      center: true,
      sortable: true,
      wrap: true,
    },
    {
      name: "Day",
      selector: (row, index): any => row.Day,
      width: "auto",
      center: true,
      sortable: true,
      wrap: true,
    },
    {
      name: "Month",
      selector: (row, index): any => row.Month,
      width: "auto",
      center: true,
      sortable: true,
      wrap: true,
    },
    {
      name: "Year",
      selector: (row, index): any => row.Year,
      width: "auto",
      center: true,
      sortable: true,
      wrap: true,
    },
    {
      name: "Realtime",
      selector: (row, index): any => row.Realtime,
      width: "auto",
      center: true,
      sortable: true,
      wrap: true,
    },
    {
      name: "Value",
      selector: (row, index): any => row.Value,
      width: "auto",
      center: true,
      sortable: true,
      wrap: true,
    },
  ];

  const handleSetData = async () => {
    console.log(selectedCategory);
    if (selectedCategory.Id === 1) {
      await setTotalRows(5);
      await setDatas(E20Data);
    }
    if (selectedCategory.Id === 2) {
      await setTotalRows(5);
      await setDatas(Bensin95Data);
    }
  };
  useEffect(() => {
    handleSetData();
  }, [selectedCategory]);

  return (
    <div className=" w-full h-screen ">
      <h1 className="text-center text-[22px] py-2 font-bold">
        Forecasting System of Oil Refinery using Time Series Techniques
      </h1>
      <div className="flex w-full justify-between pr-2 items-center">
        <div className="w-[200px] px-2">
          <p className="py-[1px] font-bold">Category Data</p>
          <ListBoxComponent
            selectedCategory={selectedCategory}
            setSelectedCategory={setSelectedCategory}
          />
        </div>
        <div className="border-[1px] w-[200px] h-[100px] rounded-md p-2 shadow-sm bg-[#DC7633] text-white">
          <p className="font-bold ">{selectedCategory.name}</p>
          <p className="text-center py-4 text-2xl font-bold ">
            {selectedCategory.Id === 1 ? E20Data.length : Bensin95Data.length}
          </p>
        </div>
      </div>

      <div className="w-full my-2 px-2">
        <p className="font-bold"> Data table for {selectedCategory.name}</p>
        <TableComponent
          columns={dataColumns}
          data={Datas}
          totalRows={totalRows}
        />
      </div>
      <div className="flex ">
        <div className="w-full ">
          <p className="text-center text-2xl ">
            Moving Average {selectedCategory.name}
          </p>
          <BarchartComponent
            data={selectedCategory.Id === 1 ? ChartE20Data : Chart95Data}
          />
        </div>
        <div className="w-full ">
          <p className="text-center text-2xl ">
            Single Exponential Smoothing {selectedCategory.name}
          </p>
          <BarchartComponent
            data={selectedCategory.Id === 1 ? ChartE20DataSingle : Chart95DataSingle}
          />
        </div>
      </div>
      <div className="flex">
       
        <div className="w-full ">
          <p className="text-center text-2xl ">
          Double Exponential Smoothing {selectedCategory.name}
          </p>
          <BarchartComponent
            data={selectedCategory.Id === 1 ? ChartE20DataDecom : Chart95DataDecom}
          />
        </div>
        <div className="w-full ">
          <p className="text-center text-2xl ">
            Decomposition {selectedCategory.name}
          </p>
          <BarchartComponent
            data={selectedCategory.Id === 1 ? ChartE20DataDouble : Chart95DataDouble}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
