import { Listbox } from '@headlessui/react'
import React from 'react'
import { CategoryInterface } from '../interface/category.interface';
import {CgSelect} from 'react-icons/cg'
import { CategoryData } from '../mock/category.mock';

const ListBoxComponent = ({selectedCategory,setSelectedCategory}:{selectedCategory:CategoryInterface,setSelectedCategory:(data:CategoryInterface)=>void}) => {
  return (
    <Listbox value={selectedCategory} onChange={setSelectedCategory}>
        <Listbox.Button className={` bg-black/25 w-full rounded-sm flex items-center justify-between px-2 text-md`}>
          {selectedCategory.name}
          <CgSelect/>
        </Listbox.Button>
        <Listbox.Options className={`border-[1px] mt-2 rounded-sm shadow-sm`}>
          {
            CategoryData.map((data:CategoryInterface)=>{
              return <Listbox.Option key={data.Id} value={data} className={''}>
              {({ active, selected }) => (
                <div
                  className={` px-2 py-[1px] ${
                    active ? ' bg-black/5 ' : 'bg-white text-black'
                  }`}
                >
                  {data.name}
                </div>
              )}
            </Listbox.Option>
            })
          }
        </Listbox.Options>

    </Listbox>
  )
}

export default ListBoxComponent