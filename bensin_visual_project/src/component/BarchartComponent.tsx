import React from "react";
import {
  Chart as ChartJS,
  LinearScale,
  CategoryScale,
  BarElement,
  PointElement,
  LineElement,
  Legend,
  Tooltip,
  Title,
  Filler,
} from "chart.js";
import { Line } from "react-chartjs-2";
ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Filler,
  Legend
);
const options = {
  scales: {
    x: {
      ticks: {
        font: {
          size: 16,
        },
      },
    },
    y: {
      ticks: {
        font: {
          size: 16,
        },
      },
    },
  },
  responsive: true,
  plugins: {
    legend: {
      position: "top" as const,
    },
    title: {
      display: true,
      text: "Chart.js Line Chart",
    },
  },
};
const BarchartComponent = ({ data }: { data: any }) => {
  return (
    <div>
      <Line options={options} data={data} />
    </div>
  );
};

export default BarchartComponent;
