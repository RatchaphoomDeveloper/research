import React from "react";
import DataTable, { TableColumn } from "react-data-table-component";

const TableComponent = ({
  columns,
  totalRows,
  data,
}: {
  columns: TableColumn<any>[];
  totalRows: number;
  data: any;
}) => {
  return (
    <>
      <DataTable
        columns={columns}
        data={data}
        pagination
       
      />
    </>
  );
};

export default TableComponent;
