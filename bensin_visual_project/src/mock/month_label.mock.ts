export const labels: string[] = [
    '1-January',
    '2-January',
    '1-February',
    '2-February',
    '1-March',
    '2-March',
    '1-April',
    '2-April',
    '1-May',
    '2-May',
    '1-June',
    '2-June',
]

export const labels95:string[] = [
    '1-July',
    '2-July',
    '1-August',
    '2-August',
    '1-September',
    '2-September',
    '1-October',
    '2-October',
    '1-November',
    '2-November',
    '1-December',
    '2-December'
]