import { labels, labels95 } from './month_label.mock';
import { DataE20Forecast, Data95Forecast, DataE20ForecastSingle, DataE20ForecastDecom, Data95ForecastDecom, Data95ForecastDouble, DataE20ForecastDouble, Data95ForecastSingle } from './forcast_data.mock';
export const ChartE20Data = {
  labels,
  datasets: [
    {
      // type: 'bar' as const,
      label: 'Test',
      data: labels.map((_, i) => DataE20Forecast.filter(x => x.Id === (i + 1))[0].TestData),
      borderColor: "rgb(53, 162, 235)",
      backgroundColor: "rgba(133, 193, 233)",
      fill: "origin",

    },
    {
      // type: 'line' as const,
      label: 'Forecast',
      data: labels.map((_, i) => DataE20Forecast.filter(x => x.Id === (i + 1))[0].ForecastData),
      borderColor: "rgb(255, 99, 132)",
      backgroundColor: "rgba(255, 0, 0)",
      fill: {
        target: "origin", // Set the fill options
        above: "rgba(255, 0, 0, 0.3)"
      }
    },

  ]
}

export const Chart95Data = {
  labels: [
    ...labels95
  ],
  datasets: [
    {
      label: 'Forecast',
      data: labels95.map((_, i) => Data95Forecast.filter(x => x.Id === (i + 1))[0].ForecastData),
      borderColor: "rgb(255, 99, 132)",
      backgroundColor: "rgba(255, 0, 0)",
      fill: {
        target: "origin", // Set the fill options
        above: "rgba(255, 0, 0, 0.3)"
      }
    },
    {
      label: 'Test',
      borderColor: "rgb(53, 162, 235)",
      backgroundColor: "rgba(53, 162, 235, 0.5)",
      fill: "origin",
      data: labels95.map((_, i) => Data95Forecast.filter(x => x.Id === (i + 1))[0].TestData),

    },
  ]
}

export const ChartE20DataSingle = {
  labels,
  datasets: [
    {
      // type: 'bar' as const,
      label: 'Test',
      data: labels.map((_, i) => DataE20ForecastSingle.filter(x => x.Id === (i + 1))[0].TestData),
      borderColor: "rgb(69, 179, 157)",
      backgroundColor: "rgba(17, 120, 100)",
      fill: "origin",

    },
    {
      // type: 'line' as const,
      label: 'Forecast',
      data: labels.map((_, i) => DataE20ForecastSingle.filter(x => x.Id === (i + 1))[0].ForecastData),
      borderColor: "rgb(255, 99, 132)",
      backgroundColor: "rgba(255, 0, 0)",
      fill: {
        target: "origin", // Set the fill options
        above: "rgba(255, 0, 0, 0.3)"
      }
    },

  ]
}

export const Chart95DataSingle = {
  labels: [
    ...labels95
  ],
  datasets: [
    {
      // type: 'bar' as const,
      label: 'Test',
      data: labels.map((_, i) => Data95ForecastSingle.filter(x => x.Id === (i + 1))[0].TestData),
      borderColor: "rgb(53, 162, 235)",
      backgroundColor: "rgba(53, 162, 235, 0.5)",
      fill: "origin",

    },
    {
      // type: 'line' as const,
      label: 'Forecast',
      data: labels.map((_, i) => Data95ForecastSingle.filter(x => x.Id === (i + 1))[0].ForecastData),
      borderColor: "rgb(255, 99, 132)",
      backgroundColor: "rgba(255, 0, 0)",
      fill: {
        target: "origin", // Set the fill options
        above: "rgba(255, 0, 0, 0.3)"
      }
    },

  ]
}

export const ChartE20DataDouble = {
  labels,
  datasets: [
    {
      // type: 'bar' as const,
      label: 'Test',
      data: labels.map((_, i) => DataE20ForecastDouble.filter(x => x.Id === (i + 1))[0].TestData),
      borderColor: "rgb(237, 187, 153)",
      backgroundColor: "rgba(211, 84, 0)",
      fill: "origin",

    },
    {
      // type: 'line' as const,
      label: 'Forecast',
      data: labels.map((_, i) => DataE20ForecastDouble.filter(x => x.Id === (i + 1))[0].ForecastData),
      borderColor: "rgb(255, 99, 132)",
      backgroundColor: "rgba(255, 0, 0)",
      fill: {
        target: "origin", // Set the fill options
        above: "rgba(255, 0, 0, 0.3)"
      }
    },

  ]
}

export const Chart95DataDouble = {
  labels: [
    ...labels95
  ],
  datasets: [
    {
      // type: 'bar' as const,
      label: 'Test',
      data: labels.map((_, i) => Data95ForecastDouble.filter(x => x.Id === (i + 1))[0].TestData),
      borderColor: "rgb(53, 162, 235)",
      backgroundColor: "rgba(53, 162, 235, 0.5)",
      fill: "origin",

    },
    {
      // type: 'line' as const,
      label: 'Forecast',
      data: labels.map((_, i) => Data95ForecastDouble.filter(x => x.Id === (i + 1))[0].ForecastData),
      borderColor: "rgb(255, 99, 132)",
      backgroundColor: "rgba(255, 0, 0)",
      fill: {
        target: "origin", // Set the fill options
        above: "rgba(255, 0, 0, 0.3)"
      }
    },

  ]
}

export const ChartE20DataDecom = {
  labels,
  datasets: [
    {
      // type: 'bar' as const,
      label: 'Test',
      data: labels.map((_, i) => DataE20ForecastDecom.filter(x => x.Id === (i + 1))[0].TestData),
      borderColor: "rgb(205, 92, 92)",
      backgroundColor: "rgba(240, 128, 128)",
      fill: "origin",

    },
    {
      // type: 'line' as const,
      label: 'Forecast',
      data: labels.map((_, i) => DataE20ForecastDecom.filter(x => x.Id === (i + 1))[0].ForecastData),
      borderColor: "rgb(255, 99, 132)",
      backgroundColor: "rgba(255, 0, 0)",
      fill: {
        target: "origin", // Set the fill options
        above: "rgba(255, 160, 122)"
      }
    },

  ]
}

export const Chart95DataDecom = {
  labels: [
    ...labels95
  ],
  datasets: [
    {
      // type: 'bar' as const,
      label: 'Test',
      data: labels.map((_, i) => Data95ForecastDecom.filter(x => x.Id === (i + 1))[0].TestData),
      borderColor: "rgb(53, 162, 235)",
      backgroundColor: "rgba(53, 162, 235, 0.5)",
      fill: "origin",

    },
    {
      // type: 'line' as const,
      label: 'Forecast',
      data: labels.map((_, i) => Data95ForecastDecom.filter(x => x.Id === (i + 1))[0].ForecastData),
      borderColor: "rgb(255, 99, 132)",
      backgroundColor: "rgba(255, 0, 0)",
      fill: {
        target: "origin", // Set the fill options
        above: "rgba(255, 0, 0, 0.3)"
      }
    },

  ]
}