import { DataInterface } from '../interface/data.interface';
export const E20Data:DataInterface[] = [
    {
        "Day": 1,
        "Month": 1,
        "Year": 2019,
        "Realtime": 2629110.333,
        "Value": 2.6291103330000003
      },
      {
        "Day": 2,
        "Month": 1,
        "Year": 2019,
        "Realtime": 2595445.188,
        "Value": 2.5954451880000002
      },
      {
        "Day": 1,
        "Month": 2,
        "Year": 2019,
        "Realtime": 2506115.467,
        "Value": 2.5061154670000003
      },
      {
        "Day": 2,
        "Month": 2,
        "Year": 2019,
        "Realtime": 4146658,
        "Value": 4.146658
      },
      {
        "Day": 1,
        "Month": 3,
        "Year": 2019,
        "Realtime": 3158104.6,
        "Value": 3.1581046
      },
      {
        "Day": 2,
        "Month": 3,
        "Year": 2019,
        "Realtime": 2705117.688,
        "Value": 2.705117688
      },
      {
        "Day": 1,
        "Month": 4,
        "Year": 2019,
        "Realtime": 2362182.333,
        "Value": 2.3621823330000002
      },
      {
        "Day": 2,
        "Month": 4,
        "Year": 2019,
        "Realtime": 3531794.4,
        "Value": 3.5317944
      },
      {
        "Day": 1,
        "Month": 5,
        "Year": 2019,
        "Realtime": 3852182.4,
        "Value": 3.8521824
      },
      {
        "Day": 2,
        "Month": 5,
        "Year": 2019,
        "Realtime": 2610328.813,
        "Value": 2.610328813
      },
      {
        "Day": 1,
        "Month": 6,
        "Year": 2019,
        "Realtime": 2533377.533,
        "Value": 2.533377533
      },
      {
        "Day": 2,
        "Month": 6,
        "Year": 2019,
        "Realtime": 2733094.467,
        "Value": 2.7330944670000004
      },
      {
        "Day": 1,
        "Month": 7,
        "Year": 2019,
        "Realtime": 4126117.333,
        "Value": 4.126117333
      },
      {
        "Day": 2,
        "Month": 7,
        "Year": 2019,
        "Realtime": 2912801.125,
        "Value": 2.912801125
      },
      {
        "Day": 1,
        "Month": 8,
        "Year": 2019,
        "Realtime": 2661645.733,
        "Value": 2.661645733
      },
      {
        "Day": 2,
        "Month": 8,
        "Year": 2019,
        "Realtime": 2377135.688,
        "Value": 2.377135688
      },
      {
        "Day": 1,
        "Month": 9,
        "Year": 2019,
        "Realtime": 3954400.733,
        "Value": 3.954400733
      },
      {
        "Day": 2,
        "Month": 9,
        "Year": 2019,
        "Realtime": 3318998.8,
        "Value": 3.3189987999999997
      },
      {
        "Day": 1,
        "Month": 10,
        "Year": 2019,
        "Realtime": 2736188.5,
        "Value": 2.7361885
      },
      {
        "Day": 2,
        "Month": 10,
        "Year": 2019,
        "Realtime": 2428106.688,
        "Value": 2.428106688
      },
      {
        "Day": 1,
        "Month": 11,
        "Year": 2019,
        "Realtime": 3263812,
        "Value": 3.263812
      },
      {
        "Day": 2,
        "Month": 11,
        "Year": 2019,
        "Realtime": 4027327.2,
        "Value": 4.0273272
      },
      {
        "Day": 1,
        "Month": 12,
        "Year": 2019,
        "Realtime": 2629110.333,
        "Value": 2.6291103330000003
      },
      {
        "Day": 2,
        "Month": 12,
        "Year": 2020,
        "Realtime": 2595445.188,
        "Value": 2.5954451880000002
      },
      {
        "Day": 1,
        "Month": 1,
        "Year": 2020,
        "Realtime": 2506115.467,
        "Value": 2.5061154670000003
      },
      {
        "Day": 2,
        "Month": 1,
        "Year": 2020,
        "Realtime": 4124462.125,
        "Value": 4.124462125
      },
      {
        "Day": 1,
        "Month": 2,
        "Year": 2020,
        "Realtime": 2999423.8,
        "Value": 2.9994237999999998
      },
      {
        "Day": 2,
        "Month": 2,
        "Year": 2020,
        "Realtime": 2702867,
        "Value": 2.702867
      },
      {
        "Day": 1,
        "Month": 3,
        "Year": 2020,
        "Realtime": 2387749.2,
        "Value": 2.3877492
      },
      {
        "Day": 2,
        "Month": 3,
        "Year": 2020,
        "Realtime": 3448753.438,
        "Value": 3.4487534380000002
      },
      {
        "Day": 1,
        "Month": 4,
        "Year": 2020,
        "Realtime": 3852182.4,
        "Value": 3.8521824
      },
      {
        "Day": 2,
        "Month": 4,
        "Year": 2020,
        "Realtime": 2604332.6,
        "Value": 2.6043326
      },
      {
        "Day": 1,
        "Month": 5,
        "Year": 2020,
        "Realtime": 2561232.733,
        "Value": 2.561232733
      },
      {
        "Day": 2,
        "Month": 5,
        "Year": 2020,
        "Realtime": 2704928.813,
        "Value": 2.704928813
      },
      {
        "Day": 1,
        "Month": 6,
        "Year": 2020,
        "Realtime": 4126117.333,
        "Value": 4.126117333
      },
      {
        "Day": 2,
        "Month": 6,
        "Year": 2020,
        "Realtime": 2923813.333,
        "Value": 2.923813333
      }
]

export const Bensin95Data:DataInterface[] = [
    {
        "Day": 1,
        "Month": 1,
        "Year": 2019,
        "Realtime": 1270864,
        "Value": 1.270864
      },
      {
        "Day": 2,
        "Month": 1,
        "Year": 2019,
        "Realtime": 1146017,
        "Value": 1.146017
      },
      {
        "Day": 1,
        "Month": 2,
        "Year": 2019,
        "Realtime": 1524480,
        "Value": 1.52448
      },
      {
        "Day": 2,
        "Month": 2,
        "Year": 2019,
        "Realtime": 1524480,
        "Value": 1.52448
      },
      {
        "Day": 1,
        "Month": 3,
        "Year": 2019,
        "Realtime": 1245681,
        "Value": 1.245681
      },
      {
        "Day": 2,
        "Month": 3,
        "Year": 2019,
        "Realtime": 4174315,
        "Value": 4.174315
      },
      {
        "Day": 1,
        "Month": 4,
        "Year": 2019,
        "Realtime": 1127274,
        "Value": 1.127274
      },
      {
        "Day": 2,
        "Month": 4,
        "Year": 2019,
        "Realtime": 6516894,
        "Value": 6.516894
      },
      {
        "Day": 1,
        "Month": 5,
        "Year": 2019,
        "Realtime": 2351462,
        "Value": 2.351462
      },
      {
        "Day": 2,
        "Month": 5,
        "Year": 2019,
        "Realtime": 1416831,
        "Value": 1.416831
      },
      {
        "Day": 1,
        "Month": 6,
        "Year": 2019,
        "Realtime": 2197221,
        "Value": 2.197221
      },
      {
        "Day": 2,
        "Month": 6,
        "Year": 2019,
        "Realtime": 1666873,
        "Value": 1.666873
      },
      {
        "Day": 1,
        "Month": 7,
        "Year": 2019,
        "Realtime": 1524752,
        "Value": 1.524752
      },
      {
        "Day": 2,
        "Month": 7,
        "Year": 2019,
        "Realtime": 3031015,
        "Value": 3.031015
      },
      {
        "Day": 1,
        "Month": 8,
        "Year": 2019,
        "Realtime": 3299806,
        "Value": 3.299806
      },
      {
        "Day": 2,
        "Month": 8,
        "Year": 2019,
        "Realtime": 602370,
        "Value": 0.60237
      },
      {
        "Day": 1,
        "Month": 9,
        "Year": 2019,
        "Realtime": 1247447,
        "Value": 1.247447
      },
      {
        "Day": 2,
        "Month": 9,
        "Year": 2019,
        "Realtime": 2066125,
        "Value": 2.066125
      },
      {
        "Day": 1,
        "Month": 10,
        "Year": 2019,
        "Realtime": 579008,
        "Value": 0.579008
      },
      {
        "Day": 2,
        "Month": 10,
        "Year": 2019,
        "Realtime": 1419524,
        "Value": 1.419524
      },
      {
        "Day": 1,
        "Month": 11,
        "Year": 2019,
        "Realtime": 2641485,
        "Value": 2.641485
      },
      {
        "Day": 2,
        "Month": 11,
        "Year": 2019,
        "Realtime": 2924746,
        "Value": 2.924746
      },
      {
        "Day": 1,
        "Month": 12,
        "Year": 2019,
        "Realtime": 4966211,
        "Value": 4.966211
      },
      {
        "Day": 2,
        "Month": 12,
        "Year": 2019,
        "Realtime": 3552612,
        "Value": 3.552612
      },
      {
        "Day": 1,
        "Month": 1,
        "Year": 2020,
        "Realtime": 1483987,
        "Value": 1.483987
      },
      {
        "Day": 2,
        "Month": 1,
        "Year": 2020,
        "Realtime": 1655001,
        "Value": 1.655001
      },
      {
        "Day": 1,
        "Month": 2,
        "Year": 2020,
        "Realtime": 2259244,
        "Value": 2.259244
      },
      {
        "Day": 2,
        "Month": 2,
        "Year": 2020,
        "Realtime": 1627947,
        "Value": 1.627947
      },
      {
        "Day": 1,
        "Month": 3,
        "Year": 2020,
        "Realtime": 633761,
        "Value": 0.633761
      },
      {
        "Day": 2,
        "Month": 3,
        "Year": 2020,
        "Realtime": 5453282,
        "Value": 5.453282
      },
      {
        "Day": 1,
        "Month": 4,
        "Year": 2020,
        "Realtime": 798618,
        "Value": 0.798618
      },
      {
        "Day": 2,
        "Month": 4,
        "Year": 2020,
        "Realtime": 1141958,
        "Value": 1.141958
      },
      {
        "Day": 1,
        "Month": 5,
        "Year": 2020,
        "Realtime": 1673197,
        "Value": 1.673197
      },
      {
        "Day": 2,
        "Month": 5,
        "Year": 2020,
        "Realtime": 856854,
        "Value": 0.856854
      },
      {
        "Day": 1,
        "Month": 6,
        "Year": 2020,
        "Realtime": 1079995,
        "Value": 1.079995
      },
      {
        "Day": 2,
        "Month": 6,
        "Year": 2020,
        "Realtime": 1486678,
        "Value": 1.486678
      },
      {
        "Day": 1,
        "Month": 7,
        "Year": 2020,
        "Realtime": 4309695,
        "Value": 4.309695
      },
      {
        "Day": 2,
        "Month": 7,
        "Year": 2020,
        "Realtime": 1883943,
        "Value": 1.883943
      },
      {
        "Day": 1,
        "Month": 8,
        "Year": 2020,
        "Realtime": 6514483,
        "Value": 6.514483
      },
      {
        "Day": 2,
        "Month": 8,
        "Year": 2020,
        "Realtime": 1971749,
        "Value": 1.971749
      },
      {
        "Day": 1,
        "Month": 9,
        "Year": 2020,
        "Realtime": 955671,
        "Value": 0.955671
      },
      {
        "Day": 2,
        "Month": 9,
        "Year": 2020,
        "Realtime": 2687334,
        "Value": 2.687334
      },
      {
        "Day": 1,
        "Month": 10,
        "Year": 2020,
        "Realtime": 1603513,
        "Value": 1.603513
      },
      {
        "Day": 2,
        "Month": 10,
        "Year": 2020,
        "Realtime": 925741,
        "Value": 0.925741
      },
      {
        "Day": 1,
        "Month": 11,
        "Year": 2020,
        "Realtime": 5418279,
        "Value": 5.418279
      },
      {
        "Day": 2,
        "Month": 11,
        "Year": 2020,
        "Realtime": 929667,
        "Value": 0.929667
      },
      {
        "Day": 1,
        "Month": 12,
        "Year": 2020,
        "Realtime": 1763256,
        "Value": 1.763256
      },
      {
        "Day": 2,
        "Month": 12,
        "Year": 2020,
        "Realtime": 1126199,
        "Value": 1.126199
      }
]