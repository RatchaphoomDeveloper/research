import { ForecastInterface } from "../interface/forecast.interface"

export const DataE20Forecast: ForecastInterface[] = [
    {
        Id: 1,
        TestData: 2.59,
        ForecastData: 3.03466
    },
    {
        Id: 2,
        TestData: 2.50,
        ForecastData: 3.03466
    },
    {
        Id: 3,
        TestData: 4.12,
        ForecastData: 3.03466
    },
    {
        Id: 4,
        TestData: 2.99,
        ForecastData: 3.03466
    },
    {
        Id: 5,
        TestData: 2.70,
        ForecastData: 3.03466
    },
    {
        Id: 6,
        TestData: 2.38,
        ForecastData: 3.03466
    },
    {
        Id: 7,
        TestData: 3.44,
        ForecastData: 3.03466
    },
    {
        Id: 8,
        TestData: 3.85,
        ForecastData: 3.03466
    },
    {
        Id: 9,
        TestData: 2.60,
        ForecastData: 3.03466
    },
    {
        Id: 10,
        TestData: 2.56,
        ForecastData: 3.03466
    },
    {
        Id: 11,
        TestData: 2.70,
        ForecastData: 3.03466
    },
    {
        Id: 12,
        TestData: 4.12,
        ForecastData: 3.03466
    },
]

export const Data95Forecast: ForecastInterface[] = [
    {
        Id: 1,
        TestData: 4.30,
        ForecastData: 1.93127
    },
    {
        Id: 2,
        TestData: 1.88,
        ForecastData: 1.93127
    },
    {
        Id: 3,
        TestData: 6.51,
        ForecastData: 1.93127
    },
    {
        Id: 4,
        TestData: 1.97,
        ForecastData: 1.93127
    },
    {
        Id: 5,
        TestData: 0.95,
        ForecastData: 1.93127
    },
    {
        Id: 6,
        TestData: 2.68,
        ForecastData: 1.93127
    },
    {
        Id: 7,
        TestData: 1.60,
        ForecastData: 1.93127
    },
    {
        Id: 8,
        TestData: 0.92,
        ForecastData: 1.93127
    },
    {
        Id: 9,
        TestData: 5.41,
        ForecastData: 1.93127
    },
    {
        Id: 10,
        TestData: 0.92,
        ForecastData: 1.93127
    },
    {
        Id: 11,
        TestData: 1.76,
        ForecastData: 1.93127
    },
    {
        Id: 12,
        TestData: 1.12,
        ForecastData: 1.93127
    },
]

export const DataE20ForecastSingle: ForecastInterface[] = [
    {
        Id: 1,
        TestData: 2.59,
        ForecastData: 3.04105
    },
    {
        Id: 2,
        TestData: 2.50,
        ForecastData: 3.04105
    },
    {
        Id: 3,
        TestData: 4.12,
        ForecastData: 3.04105
    },
    {
        Id: 4,
        TestData: 2.99,
        ForecastData: 3.04105
    },
    {
        Id: 5,
        TestData: 2.70,
        ForecastData: 3.04105
    },
    {
        Id: 6,
        TestData: 2.38,
        ForecastData: 3.04105
    },
    {
        Id: 7,
        TestData: 3.44,
        ForecastData: 3.04105
    },
    {
        Id: 8,
        TestData: 3.85,
        ForecastData: 3.04105
    },
    {
        Id: 9,
        TestData: 2.60,
        ForecastData: 3.04105
    },
    {
        Id: 10,
        TestData: 2.56,
        ForecastData: 3.04105
    },
    {
        Id: 11,
        TestData: 2.70,
        ForecastData: 3.04105
    },
    {
        Id: 12,
        TestData: 4.12,
        ForecastData: 3.04105
    },
]

export const Data95ForecastSingle: ForecastInterface[] = [
    {
        Id: 1,
        TestData: 4.30,
        ForecastData: 2.14549
    },
    {
        Id: 2,
        TestData: 1.88,
        ForecastData: 2.14549
    },
    {
        Id: 3,
        TestData: 6.51,
        ForecastData: 2.14549
    },
    {
        Id: 4,
        TestData: 1.97,
        ForecastData: 2.14549
    },
    {
        Id: 5,
        TestData: 0.95,
        ForecastData: 2.14549
    },
    {
        Id: 6,
        TestData: 2.68,
        ForecastData: 2.14549
    },
    {
        Id: 7,
        TestData: 1.60,
        ForecastData: 2.14549
    },
    {
        Id: 8,
        TestData: 0.92,
        ForecastData: 2.14549
    },
    {
        Id: 9,
        TestData: 5.41,
        ForecastData: 2.14549
    },
    {
        Id: 10,
        TestData: 0.92,
        ForecastData: 2.14549
    },
    {
        Id: 11,
        TestData: 1.76,
        ForecastData: 2.14549
    },
    {
        Id: 12,
        TestData: 1.12,
        ForecastData: 2.14549
    },
]

export const DataE20ForecastDouble: ForecastInterface[] = [
    {
        Id: 1,
        TestData: 2.59,
        ForecastData: 3.10974
    },
    {
        Id: 2,
        TestData: 2.50,
        ForecastData:3.11358
    },
    {
        Id: 3,
        TestData: 4.12,
        ForecastData: 3.11741
    },
    {
        Id: 4,
        TestData: 2.99,
        ForecastData: 3.12125
    },
    {
        Id: 5,
        TestData: 2.70,
        ForecastData: 3.12509
    },
    {
        Id: 6,
        TestData: 2.38,
        ForecastData: 3.12892
    },
    {
        Id: 7,
        TestData: 3.44,
        ForecastData: 3.13276
    },
    {
        Id: 8,
        TestData: 3.85,
        ForecastData: 3.1366
    },
    {
        Id: 9,
        TestData: 2.60,
        ForecastData: 3.14043
    },
    {
        Id: 10,
        TestData: 2.56,
        ForecastData: 3.14427
    },
    {
        Id: 11,
        TestData: 2.70,
        ForecastData: 3.14811
    },
    {
        Id: 12,
        TestData: 4.12,
        ForecastData: 3.15194
    },
]

export const Data95ForecastDouble: ForecastInterface[] = [
    {
        Id: 1,
        TestData: 4.30,
        ForecastData: 2.18454
    },
    {
        Id: 2,
        TestData: 1.88,
        ForecastData: 2.18292
    },
    {
        Id: 3,
        TestData: 6.51,
        ForecastData: 2.18129
    },
    {
        Id: 4,
        TestData: 1.97,
        ForecastData: 2.17966
    },
    {
        Id: 5,
        TestData: 0.95,
        ForecastData: 2.17803
    },
    {
        Id: 6,
        TestData: 2.68,
        ForecastData: 2.17641
    },
    {
        Id: 7,
        TestData: 1.60,
        ForecastData: 2.17478
    },
    {
        Id: 8,
        TestData: 0.92,
        ForecastData: 2.17315
    },
    {
        Id: 9,
        TestData: 5.41,
        ForecastData: 2.17153
    },
    {
        Id: 10,
        TestData: 0.92,
        ForecastData: 2.1699
    },
    {
        Id: 11,
        TestData: 1.76,
        ForecastData: 2.16827
    },
    {
        Id: 12,
        TestData: 1.12,
        ForecastData: 2.16665
    },
]

export const DataE20ForecastDecom: ForecastInterface[] = [
    {
        Id: 1,
        TestData: 2.59,
        ForecastData: 2.71331
    },
    {
        Id: 2,
        TestData: 2.50,
        ForecastData: 2.42771
    },
    {
        Id: 3,
        TestData: 4.12,
        ForecastData: 3.2479
    },
    {
        Id: 4,
        TestData: 2.99,
        ForecastData: 3.98295
    },
    {
        Id: 5,
        TestData: 2.70,
        ForecastData: 2.77684
    },
    {
        Id: 6,
        TestData: 2.38,
        ForecastData: 2.65877
    },
    {
        Id: 7,
        TestData: 3.44,
        ForecastData: 2.38371
    },
    {
        Id: 8,
        TestData: 3.85,
        ForecastData: 3.88951
    },
    {
        Id: 9,
        TestData: 2.60,
        ForecastData: 3.27613
    },
    {
        Id: 10,
        TestData: 2.56,
        ForecastData:2.71291
    },
    {
        Id: 11,
        TestData: 2.70,
        ForecastData: 2.42735
    },
    {
        Id: 12,
        TestData: 4.12,
        ForecastData: 3.24742
    },
]

export const Data95ForecastDecom: ForecastInterface[] = [
    {
        Id: 1,
        TestData: 4.30,
        ForecastData: 1.79698
    },
    {
        Id: 2,
        TestData: 1.88,
        ForecastData: 2.27754
    },
    {
        Id: 3,
        TestData: 6.51,
        ForecastData: 4.81589
    },
    {
        Id: 4,
        TestData: 1.97,
        ForecastData: 1.88335
    },
    {
        Id: 5,
        TestData: 0.95,
        ForecastData: 0.99478
    },
    {
        Id: 6,
        TestData: 2.68,
        ForecastData: 2.77765
    },
    {
        Id: 7,
        TestData: 1.60,
        ForecastData: 1.15676
    },
    {
        Id: 8,
        TestData: 0.92,
        ForecastData: 1.63166
    },
    {
        Id: 9,
        TestData: 5.41,
        ForecastData: 2.50757
    },
    {
        Id: 10,
        TestData: 0.92,
        ForecastData: 1.48657
    },
    {
        Id: 11,
        TestData: 1.76,
        ForecastData: 2.33774
    },
    {
        Id: 12,
        TestData: 1.12,
        ForecastData: 1.83677
    },
]