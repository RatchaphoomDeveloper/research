export interface DataInterface {
    Id?: number;
    Day?: number;
    Month?: number;
    Year?: number;
    Date?: Date;
    Realtime?: number;
    Value?: number;
}