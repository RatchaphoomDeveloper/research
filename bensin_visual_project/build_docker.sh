source .env

docker buildx build --platform linux/amd64 --no-cache  -t $WEBAPINAME:$WEBAPIVERSION -f ./Dockerfile .
docker image tag $WEBAPINAME:$WEBAPIVERSION $GITLAB_PATH/$WEBAPINAME:$WEBAPIVERSION
docker push $GITLAB_PATH/$WEBAPINAME:$WEBAPIVERSION